﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using AlbumPhoto.Models;
using Models;
using System.IO;
using BL.services;

namespace AlbumPhoto.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAlbumBl _AlbumBL;
        public HomeController(IAlbumBl AlbumBL)
        {
            this._AlbumBL = AlbumBL;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public IActionResult Create(AlbumVM model)
        {
            if (!ModelState.IsValid)
            {
                return View("Create", model);
            }

            var guid = Guid.NewGuid().ToString();
            var photoName = $"{guid}_{model.PhotoLink.FileName}";
            var FilePath = $"wwwroot\\uploads\\{photoName}";
            using (var stream = new FileStream(FilePath, FileMode.Create))
            {
                model.PhotoLink.CopyTo(stream);
            }

            var album = new Album()
            {
                Name = model.Name,
                Description = model.Description,
                PhotoLink = $"/uploads/{photoName}",
            };

            _AlbumBL.Create(album);

            return RedirectToAction("Index");
        }
    }
}
