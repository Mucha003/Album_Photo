﻿using BL.services;
using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL
{
    public class PhotoBL : IPhotoBL
    {
        private readonly AlbumDbContext _db;
        public PhotoBL(AlbumDbContext context)
        {
            _db = context;
        }


        public bool Create(Photos model)
        {
            try
            {
                model.CreateAt = DateTime.Now;
                _db.Photos.Add(model);
                _db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
