﻿using BL.services;
using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL
{
    public class AlbumBL : IAlbumBl
    {
        private AlbumDbContext _db;
        public AlbumBL(AlbumDbContext dbContext)
        {
            _db = dbContext;
        }

        public bool Create(Album model)
        {
            try
            {
                model.CreateAt = DateTime.Now;
                _db.Album.Add(model);
                _db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Album Get(int AlbumId)
        {
            var resultat = new Album();
            try
            {
                resultat = _db.Album.Include(x => x.Photos)
                                    .FirstOrDefault(x => x.ID == AlbumId);
            }
            catch (Exception)
            {
            }
            return resultat;
        }

        public List<Album> GetAll()
        {
            var resultat = new List<Album>();
            try
            {
                resultat = _db.Album.OrderByDescending(x => x.CreateAt).ToList();
            }
            catch (Exception)
            {
            }
            return resultat;
        }
    }
}
