﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class Album
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string PhotoLink { get; set; }
        public DateTime CreateAt { get; set; }

        public ICollection<Photos> Photos { get; set; }
    }
}
