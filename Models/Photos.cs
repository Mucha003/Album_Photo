﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class Photos
    {
        public int ID { get; set; }
        public string PhotoLink { get; set; }
        public DateTime CreateAt { get; set; }

        public int AlbumID { get; set; }
        public virtual Album Album { get; set; }
    }
}
