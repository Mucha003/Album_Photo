﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AlbumPhoto.Models
{
    public class PhotoMV
    {
        [Required]
        public int AlbumId { get; set; }

        [Required]
        public IFormFile PhotoLink { get; set; }
    }
}
