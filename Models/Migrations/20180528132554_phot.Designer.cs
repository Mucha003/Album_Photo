﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using Models;
using System;

namespace Models.Migrations
{
    [DbContext(typeof(AlbumDbContext))]
    [Migration("20180528132554_phot")]
    partial class phot
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.3-rtm-10026")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Models.Album", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateAt");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<string>("PhotoLink");

                    b.HasKey("ID");

                    b.ToTable("Album");
                });

            modelBuilder.Entity("Models.Photos", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AlbumID");

                    b.Property<DateTime>("CreateAt");

                    b.Property<string>("PhotoLink");

                    b.HasKey("ID");

                    b.HasIndex("AlbumID");

                    b.ToTable("Photos");
                });

            modelBuilder.Entity("Models.Photos", b =>
                {
                    b.HasOne("Models.Album", "Album")
                        .WithMany("Photos")
                        .HasForeignKey("AlbumID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
