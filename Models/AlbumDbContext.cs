﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class AlbumDbContext : DbContext
    {
        public AlbumDbContext(DbContextOptions<AlbumDbContext> op)
            : base(op)
        {
        }

        public DbSet<Album> Album { get; set; }
        public DbSet<Photos> Photos { get; set; }
    }
}
