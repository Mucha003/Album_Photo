﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using AlbumPhoto.Models;
using Models;
using System.IO;
using BL.services;

namespace AlbumPhoto.Controllers
{
    public class PhotoController : Controller
    {
        private readonly IAlbumBl _AlbumBL;
        private readonly IPhotoBL _PhotoBL;
        public PhotoController(IAlbumBl context, IPhotoBL PhotoBL)
        {
            this._AlbumBL = context;
            this._PhotoBL = PhotoBL;
        }

        public IActionResult Index(int id)
        {
            var album = _AlbumBL.Get(id);
            return View(album);
        }

        [HttpPost]
        public IActionResult Create(PhotoMV model)
        {
            if (!ModelState.IsValid)
            {
                return View("Create", model);
            }

            var guid = Guid.NewGuid().ToString();
            var photoName = $"{guid}_{model.PhotoLink.FileName}";
            var FilePath = $"wwwroot\\uploads\\{photoName}";
            using (var stream = new FileStream(FilePath, FileMode.Create))
            {
                model.PhotoLink.CopyTo(stream);
            }

            var picture = new Photos()
            {
                AlbumID = model.AlbumId,
                PhotoLink = $"/uploads/{photoName}",
            };

            _PhotoBL.Create(picture);

            return RedirectToAction("Index", new { id = model.AlbumId});
        }
    }
}