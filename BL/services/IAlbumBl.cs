﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL.services
{
    public interface IAlbumBl
    {
        bool Create(Album model);

        List<Album> GetAll();

        Album Get(int AlbumId);
    }
}
