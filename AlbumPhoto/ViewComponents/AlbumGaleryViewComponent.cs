﻿using BL.services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlbumPhoto.ViewComponents
{
    [ViewComponent(Name = "AlbumGalery")]
    public class AlbumGaleryViewComponent : ViewComponent
    {
        private readonly IAlbumBl _AlbumBL;
        public AlbumGaleryViewComponent(IAlbumBl repository)
        {
            _AlbumBL = repository;
        }

        public IViewComponentResult Invoke()
        {
            var liste = _AlbumBL.GetAll();
            return View(liste);
        }
    }
}
